# Editor Definition Meta-Model
Eclipse Project for the meta-model of XGEE editors. Running the generated .editor allows editing the individual XGEE editor instances. 

## Usage
See a good guide in the docs on [docs.xgee.de](https://xgee.gitlab.io/tutorial/modeling_with_xgee.html). 

## Development Guidelines
Make changes to the meta-models very carefully. If there is no very good reason, always keep backward compatibility. 

For some reason, Eclipse changes something to the .project. Don't commit changes to it, since it breaks Eclipse. See d814be for details.
