# XGEE Layout Meta-Model
Eclipse Project for the layout meta-model of XGEE editors.

![Layout meta-model opened in the Eclipse Modeling Framework](Media/LayoutMetaModel-ClassDiagram.png)

## Development Guidelines
Make changes to the meta-models very carefully. If there is no very good reason, always keep backward compatibility. 